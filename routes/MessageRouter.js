import { Router } from 'express';
import Message from '../schemas/messageschema.js';

const router = Router();

router.get('/', async (request, response) => {
  try {
    const { users, usersTwo } = request.query;

    const messages = await Message.find({
      users: { $all: [users, usersTwo] },
    });

    response.json(messages);
  } catch (error) {
    console.error('Error fetching messages:', error);
    response.status(500).send('Internal Server Error');
  }
});

router.post('/', async (request, response) => {
  try {
    const { message, users, timestamp, read } = request.body;

    if (!message || !users || !timestamp) {
      return response.status(400).send('Bad Request');
    }

    const newMessage = await Message.create({
      message,
      users,
      timestamp,
      read: read || false,
    });

    response.status(201).json(newMessage);
  } catch (error) {
    console.error('Error creating message:', error);
    response.status(500).send('Internal Server Error');
  }
});

export default router;
