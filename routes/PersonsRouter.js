import { Router } from 'express';
import User from '../schemas/personschema.js';

const router = Router();

router.get('/', async (request, response) => {
  try {
    const allUsers = await User.find();
    response.json(allUsers);
  } catch (error) {
    console.error('Error fetching users:', error);
    response.status(500).send('Internal Server Error');
  }
});

router.get('/:name', async (request, response) => {
  try {
    const name = request.params.name;
    const user = await User.findOne({ name });

    if (!user) {
      response.sendStatus(404);
    }

    response.json(user);
  } catch (error) {
    console.error('Error fetching user:', error);
    response.status(500).send('Internal Server Error');
  }
});

router.post('/', async (request, response) => {
  try {
    const { name, password } = request.body;

    const existingUser = await User.findOne({ name });

    if (existingUser) {
      return response.json(existingUser);
    }

    const newUser = await User.create({
      name,
      password,
      lastOnline: new Date().toISOString(),
    });

    response.status(201).json(newUser);
  } catch (error) {
    console.error('Error creating user:', error);
    response.status(500).send('Internal Server Error');
  }
});

router.delete('/:name', async (request, response) => {
  try {
    const name = request.params.name;
    const deletedUser = await User.findOneAndDelete({ name });

    if (!deletedUser) {
      return response.sendStatus(404);
    }

    response.sendStatus(204);
  } catch (error) {
    console.error('Error deleting user:', error);
    response.status(500).send('Internal Server Error');
  }
});

export default router;
