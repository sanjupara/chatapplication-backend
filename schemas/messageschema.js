import mongoose from 'mongoose';

const messageSchema = new mongoose.Schema({
  id: {
    type: Number,
  },
  message: {
    type: String,
  },
    users: {
        type: Array,
    }, 
  timestamp: {
    type: Date,
    required: true,
    default: Date.now,
  },
  read: {
    type: Boolean,
    default: false,
  }
});

export default mongoose.model('Message', messageSchema);