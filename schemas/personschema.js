import mongoose from 'mongoose';

const personSchema = new mongoose.Schema({
  id: {
    type: Number,
  },
  name: {
    type: String,
  },
    password: {
        type: Array,
    }, 
  lastOnline: {
    type: Date,
    required: true,
    default: Date.now,
  }
});

export default mongoose.model('Person', personSchema);