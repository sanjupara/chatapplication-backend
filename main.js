import express from 'express';
import session from 'express-session'
import { gugusRouter } from "./routes/GugusRouter.js";
import  personsRouter  from './routes/PersonsRouter.js';
import messageRouter from './routes/MessageRouter.js';
import cors from 'cors';
import mongoos from 'mongoose';
const app = express()
const port = 3000;
app.use(express.urlencoded({ "extended": true }));
app.use(express.json())
app.use(cors());

app.use(session({
    secret: "supersecret", 
    resave: false,
    saveUninitialized: true,
    cookie: {}
}));

mongoos.connect('mongodb+srv://sanan:Sanju@chatappcluster.cdfvnqy.mongodb.net/', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoos.connection

db.on('error', (error) => console.log(error))

db.once('open', () => console.log('connected to database'))

app.use("/persons", personsRouter);
app.use("/gugus", gugusRouter);
app.use("/messages", messageRouter); 


app.listen(port, () => {
    console.log("Currently running on port " + port)
});
